Bank Application

Cucu Teodora-Laura
Georgescu Vlad-Stefan
Mihai Simona-Gabriela

    Aplicatia propusa este alcatuita din:
        - un microserviciu care se ocupa de autentificare si autorizare (auth-service)
        - un microserviciu care se ocupa de business logic (business-logic-service)
        - un microserviciu care interactioneaza cu baza de date (io-service)
        - un microserviciu care are rol de gateway (apigateway / kong)
        - un utilitar de gestiune a bazei de date (adminer)
        - o baza de date PostgreSQL
        - Portainer pentru asigurarea gestiunii din UI a clusterului
        - Gitlab CI/CD pentru build si deployment automat
        
    Fiecare microserviciu este o aplicatie NodeJs.
    Proiectul include trei fisiere de configurari Docker, folosite pentru a defini si a porni serviciile pe containere in
Docker (docker-compose.yml) sau in stiva de servicii in Docker Swarm (stack-original.yml si stack-kong.yml).
    Fiecare serviciu include cate un fisier Dockerfile prin intermediul caruie se creeaza imaginile corespunzzatoare.
    In directorul Database se afla fisierul de initializare a bazei de date prin care se creaza cele doua tabele
folosite (CLIENTS si ACCOUNTS).

    Microserviciul care are rol de gateway declarativ expune rute prin care se fac request-uri la microserviciile cu
care acesta comunica (business-logic-service si auth-service) Cand este acceseaza ruta register, body-ul este trimis mai
departe la auth-service care face o inscriere noua in tabela CLIENTS. La accesarea rutei login, username-ul, parola si
email-ul introduse sunt trimise la auth-service unde se face un SELECT din tabela CLIENTS in functie de username si
email si apoi se verifica parola primita de la gateway cu cea din baza de date. Daca acestea coincid se genereaza un
token avand ca payload client_id-ul si rolul. Acest token este returnat ca raspuns la gateway.
    In cazul rutelor corespunzatoare conturilor si clientilor, gateway-ul trebuie sa primeasca token-ul generat la login
ca header de autorizare pentru a-l trimite catre business-logic-service. Acesta din urma expune in fisierul controllers.js
rutele pe care le acceseaza gateway-ul. In fiserul services.js sunt definite functii (getClients, getClientById, getAccounts,
getAccountByIban, getAccountsByClientId, addAccount, transfer, withdraw, deposit, deleteAccountByIban) care sunt apelate
din rutele corespunzatoare. Primul pas pe care il face fiecare dintre aceste functii este de a trimite tokenul primit de
la gateway catre auth-service, accesand ruta verify expusa de acest microserviciu. Acesta din urma decodifica token-ul
si verifica daca rolul incapsulat in token face parte din lista de roluri cu acces la operatia respectiva. In final,
serviciul business-logic primeste de la auth-service un boolean care decide daca utilizatorul logat poate executa operatia.
In caz pozitiv, se face un al apel catre io-service pe ruta expusa de acesta corespunzatoare operatiei dorite. Rezultatul
primit este returnat de catre functie si trimis din controller catre gateway ca raspuns.
    Serviciul io-service expune la randul sau rute pe care le acceseaza business-logic-ul. In cadrul acestor rute se
apeleaza functiile definite care executa query-uri directe asupra bazei de date.
    Pentru a face posibila aceasta conexiune intre servicii, acestea trebuie sa se afle in aceleasi retele:
        - reteaua dintre gateway si business-logic;
        - reteaua dintre gateway si auth;
        - reteaua dintre business-logic si auth;
        - reteaua dintre io si business-logic;
        - reteaua dintre io si db;
        - reteaua dintre auth si db;
        - reteaua dintre db si adminer.
    Pentru siguranta datelor, am folosit secrete. Datele mentinute ca fiind secrete sunt userul si parola bazei de date,
issuer si secret token folosite de jwt. 

    Comenzile care trebuiesc rulate dupa "docker swarm init" pentru a crea secretele sunt:

    printf "admin" | docker secret create bank-db-user-secret -
    printf "admin" | docker secret create bank-db-password-secret -
    printf "student" | docker secret create jwt-issuer-secret -
    printf "myawesomeultrasecretkey" | docker secret create jwt-secret-key-secret -


    Aplicatia poate rula pe Docker swarm, si pentru testare am folosit Play with docker. Dupa crearea secretelor putem
rula "docker stack deploy -c stack-*.yml bank-app" pentru a face deploy configuratiei specifice aplicatiei noastre si serviciile
vor fi rulate si distribuite pe nodurile din swarm, cu mentiunea ca este obligatoriu ca baza de date si kong sa ruleze pe
nodul master.
    In versiunea finala, api-gateway-ul implementat de noi a fost inlocuit de kong si a fost creata o singura retea care include
kong, business-logic si auth, retea numita internal. Kong expune rutele pentru serviciile: auth, adminer, business logic (clients 
si account). Pentru autentificare am folosit un header de authorisation care trebuie sa aiba valoarea unui token primit ca raspuns
la cererea de login. Am ales aceasta metoda deoarece am considerat ca fiecare utilizator trebuie sa primeasca dupa login un token 
care sa cripteze username ul si rolul sau - fiind astfel personalizat pentru a realiza simultan si autentificarea si autorizarea 
acestuia. 
    In plus, dupa ce am pornit serviciile din stiva de pe swarm, pornim si serviciul de portainer care ne va permite sa vedem
configuratiile serviciilor, stivelor, containerelor noastre intr-o interfata user-friendly. 
    In continuare, din gitlab  am creat un grup de repository-uri, fiecare dintre acestea reprezentand un serviciu, apoi am creat si 
inregistrat un gitlab runner de grup pentru a crea pipeline-uri pentru toate serviciile din acest grup. Pipeline-ul va avea doua
job-uri: unul de build si unul de deploy. Prin build se creeaza imaginile serviciilor si se publica in registry-ul gitlab. Prin deploy,
se updateaza containerele din swarm, respectiv imaginile serviciilor care au fost modificate, fiind luata ultima varianta.
    Ca metoda de colaborare am ales initial github, facand commmituri pe masura ce am adaugat secretele si kong, ulterior am trecut la
gitlab unde am publicat serviciile ca repository-uri intr-un grup pentru a folosi pipeline-uri.

Link catre grupul de repository-uri din Gitlab: https://gitlab.com/internetbanking-cc
